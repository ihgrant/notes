smooth surface formula: `arctan(layer_height / pixel_width)`
* printed at 0.05mm layer height
* Elegoo Saturn 2 has a 0.0285mm pixel width
* gives us a rotation of 60.32 degrees

5 prints of [N-scale boxcar](https://www.thingiverse.com/thing:5441904), all rotated on Y axis only with respect to the build plate:

| rotation | notes                                                                        |
| -------- | ---------------------------------------------------------------------------- |
| 29.68º   | significant warping on both sides                                            |
| 60.32º   | poor detail on end closest to build plate                                    |
| 90º      | significant outward deformation on build-plate side                          |
| 119.68º  | top and side closest to build plate slightly deformed with poor detail       |
| 150.32º  | top and side closest to build plate is deformed; poor detail on top of shell |
